import { GetRandomFloat, GetRandomInt, ToLuma, Clamp } from './utils';
var MaxParticleSize = 6;
var MinParticleSize = 1;
var Particle = /** @class */ (function () {
    function Particle(w, h, palette) {
        this.w = w;
        this.h = h;
        this.palette = palette;
        this.x = 0;
        this.y = 0;
        this.speed = 0;
        this.theta = 0;
        this.radius = MinParticleSize;
        this.ttl = 500;
        this.lifetime = 500;
        this.alpha = 0.8;
        this.color = 'black';
        this.reset();
    }
    Particle.prototype.reset = function () {
        this.x = GetRandomFloat(0, this.w);
        this.y = GetRandomFloat(0, this.h);
        this.speed = GetRandomFloat(0, 3.0);
        this.theta = GetRandomFloat(0, Math.PI * 2);
        this.radius = GetRandomFloat(0.05, 1.0);
        this.lifetime = this.ttl = GetRandomInt(25, 50);
        this.color = this.palette[GetRandomInt(0, this.palette.length)];
        this.ttl = this.lifetime = GetRandomInt(25, 50);
    };
    Particle.prototype.imageComplementLuma = function (imageData) {
        var p = Math.floor(this.x) + Math.floor(this.y) * imageData.width;
        // image rgba
        var i = Math.floor(p * 4);
        var r = imageData.data[i + 0];
        var g = imageData.data[i + 1];
        var b = imageData.data[i + 2];
        // const a = imageData.data[i + 3];
        var luma = ToLuma(r, g, b);
        var ln = 1 - luma / 255.0;
        return ln;
    };
    Particle.prototype.Update = function (params) {
        var dRadius = GetRandomFloat(-MaxParticleSize / 5, MaxParticleSize / 5);
        var i = params.index;
        var r = params.phase;
        var d = params.d;
        // const d = 1 / Math.pow(10, params.d);
        // const d = 0.01;
        var t = params.t;
        // form spriral
        // this.x = Math.sin(r + i * d + t) * i + this.w / 2;
        // this.y = Math.cos(r + i * d + t) * i + this.h / 2;
        // form warp like
        // this.x = Math.sin(r + i * d + t) * i + this.w / 2;
        // this.y = Math.cos(r - i * d + t) * i + this.h / 2;
        // form paperlike
        // this.x = Math.sin(r + i * d + t) * i + this.w / 2;
        // this.y = Math.cos(r - i * d - t) * i + this.h / 2;
        // form random
        // this.x = Math.sin(r + i * d - t) * i + this.w / 2;
        // this.y = Math.cos(r - i * d + t) * i + this.h / 2;
        // form random
        this.x = Math.sin(r + this.x * d - t) * i + this.w / 2;
        this.y = Math.cos(r - this.y * d + t) * i + this.h / 2;
        // form more crazy random
        // this.x = Math.cos(r + Math.sin(r + this.x * d - t)) * i + this.w / 2;
        // this.y = Math.sin(r - Math.cos(r - this.y * d + t)) * i + this.h / 2;
        // this.x = Clamp(0, this.w - 1, this.x)
        // this.y = Clamp(0, this.h - 1, this.y)
        var ln = this.imageComplementLuma(params.imageData);
        var lt = (this.lifetime - this.ttl) / this.lifetime;
        var f = ln;
        this.alpha = ln;
        this.radius += dRadius;
        this.radius = Clamp(MinParticleSize, MaxParticleSize, this.radius) * f;
        if (this.speed < 1.0) {
            this.radius = 0.1;
        }
        // lifetime
        this.ttl += -1;
        if (this.ttl === 0) {
            this.reset();
        }
    };
    Particle.prototype.Draw = function (ctx) {
        ctx.save();
        this.experiment3(ctx);
        ctx.restore();
    };
    Particle.prototype.experiment3 = function (ctx) {
        ctx.fillStyle = this.color;
        ctx.globalAlpha = this.alpha;
        var circle = new Path2D();
        circle.arc(this.x, this.y, this.radius, 0, Math.PI * 2);
        ctx.fill(circle);
    };
    return Particle;
}());
export { Particle };
