// util
export function GetRandomFloat(min, max) {
    return Math.random() * (max - min) + min;
}
export function GetRandomInt(min, max) {
    return Math.floor(GetRandomFloat(min, max));
}
export function FromPolar(v, theta) {
    return [v * Math.cos(theta), v * Math.sin(theta)];
}
export function ToLuma(r, g, b) {
    return 0.2126 * r + 0.7152 * g + 0.0722 * b;
}
export function Clamp(min, max, value) {
    return value > max ? max : (value < min ? min : value);
}
