import { Simulation } from './simulation';
import { GetRandomInt } from './utils';
function createDrawCanvas(imageCtx, width, height) {
    var _a;
    var updateFrameRate = 60;
    var renderFrameRate = 60;
    // const canvas = document.getElementById('canvas') as HTMLCanvasElement;
    var canvas = document.createElement('canvas');
    // document.body.appendChild(canvas); 
    (_a = document.getElementById('canvas')) === null || _a === void 0 ? void 0 : _a.appendChild(canvas);
    var ctx = canvas.getContext('2d');
    canvas.width = width;
    canvas.height = height;
    if (!canvas)
        return;
    if (!ctx)
        return;
    ctx.imageSmoothingEnabled = true;
    ctx.imageSmoothingQuality = 'high';
    var sim = new Simulation(width, height);
    var imageData = imageCtx.getImageData(0, 0, width, height);
    var params = {
        imageData: imageData,
        phase: 0,
        //d: GetRandomInt(0, 4),
        d: 0,
        t: 0,
    };
    setInterval(function () {
        sim.Update(params);
    }, 1000 / updateFrameRate);
    setInterval(function () {
        sim.Draw(ctx);
    }, 1000 / renderFrameRate);
}
function bootstrapper() {
    var width = 360;
    var height = 360;
    var imageCanvas = document.createElement('canvas');
    document.body.appendChild(imageCanvas);
    imageCanvas.width = width;
    imageCanvas.height = height;
    var ctx = imageCanvas.getContext('2d');
    if (!ctx)
        return;
    // image
    var image = new window.Image();
    if (!image)
        return;
    image.crossOrigin = 'Anonymous';
    image.onload = function (e) {
        ctx.drawImage(image, 0, 0, width, height);
        createDrawCanvas(ctx, width, height);
    };
    var images = [
        '../assets/images/tessy.jpg',
        '../assets/images/dhali.jpg',
        '../assets/images/monaspears.jpg',
        '../assets/images/zx10r-2004.jpg',
        '../assets/images/zx10r-logo.jpg',
        '../assets/images/fb.jpg',
        '../assets/images/yoji.jpg',
        '../assets/images/mandalorian.jpg',
        '../assets/images/buddha.jpg',
        '../assets/images/smiley.jpg'
    ];
    image.src = images[GetRandomInt(0, images.length)];
}
bootstrapper();
