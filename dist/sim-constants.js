export var ParticleCount = 400;
export var ColorPalettes = [
    ['#cdb4db', '#ffc8dd', '#ffafcc', '#bde0fe', '#a2d2ff'],
    ['#ef476f', '#ffd166', '#06d6a0', '#118ab2', '#073b4c'],
    ['#3e5641', '#a24936', '#d36135', '#282b28', '#83bca9'],
    ['#c9e4ca', '#87bba2', '#55828b', '#3b6064', '#364958'],
    ['#f8f4e3', '#d4cdc3', '#d5d0cd', '#a2a392', '#9a998c'],
    ['#2c363f', '#e75a7c', '#f2f5ea', '#d6dbd2', '#bbc7a4'],
    ['#880d1e', '#dd2d4a', '#f26a8d', '#f49cbb', '#cbeef3'],
    ['#3e442b', '#6a7062', '#8d909b', '#aaadc4', '#d6eeff'],
    ['#e63946', '#f1faee', '#a8dadc', '#457b9d', '#1d3557'],
    ['#000000', '#14213d', '#fca311', '#e5e5e5', '#ffffff'],
    ['#f6bd60', '#f7ede2', '#f5cac3', '#84a59d', '#f28482'],
    ['#ffffff', '#000000', '#ffffff', '#000000', '#ffffff'],
];
