import { ISimObject, MagicParams } from './isimobject';
import { Particle } from './particle';
import { ColorPalettes, ParticleCount } from './sim-constants';
import { GetRandomInt } from './utils';

export class Simulation implements ISimObject {
  particles: Particle[] = [];
  palette: string[] = [];
  constructor(private width: number, private height: number) { 
    this.palette = ColorPalettes[GetRandomInt(0, ColorPalettes.length)]
    for (let i = 0; i < ParticleCount; i++) {
      this.particles.push(new Particle(this.width, this.height, this.palette))
    }
  }
  dt = 1 / Math.pow(10, GetRandomInt(2, 5));
  dd = 1 / Math.pow(10, GetRandomInt(2, 5));
  Update(params: MagicParams) {
    params.phase += Math.PI / 256;
    params.t += this.dt;
    params.d = this.dd;
    this.particles.forEach((p, i) => {
      params.index = i;
      p.Update(params);
    })
  }

  init = false;
  Draw(ctx: CanvasRenderingContext2D) {
    if (!this.init) {
      ctx.fillStyle = this.palette[0];
      ctx.fillRect(0, 0, this.width, this.height);
      this.init = true;
    }


    this.particles.forEach( p => p.Draw(ctx))
  }
}