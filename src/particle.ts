import { ISimObject, MagicParams } from "./isimobject";
import { GetRandomFloat, GetRandomInt, ToLuma, FromPolar, Clamp } from './utils';


const MaxParticleSize = 6;
const MinParticleSize = 1;

export class Particle implements ISimObject {
  x = 0;
  y = 0;
  speed = 0;
  theta = 0;
  radius = MinParticleSize;
  ttl = 500;
  lifetime = 500;
  alpha = 0.8;

  color = 'black';

  constructor(private w: number, private h: number, private palette:string[]) {
    this.reset();
  }

  reset() {
    this.x = GetRandomFloat(0, this.w);
    this.y = GetRandomFloat(0, this.h);

    this.speed = GetRandomFloat(0, 3.0);
    this.theta = GetRandomFloat(0, Math.PI * 2);

    this.radius = GetRandomFloat(0.05, 1.0);
    this.lifetime = this.ttl = GetRandomInt(25, 50);

    this.color = this.palette[GetRandomInt(0, this.palette.length)]; 
    
    this.ttl = this.lifetime = GetRandomInt(25, 50);
  }

  imageComplementLuma(imageData: ImageData):number {
    const p = Math.floor(this.x) + Math.floor(this.y) * imageData.width;
    // image rgba
    const i = Math.floor(p * 4);
    const r = imageData.data[i + 0];
    const g = imageData.data[i + 1];
    const b = imageData.data[i + 2];
    // const a = imageData.data[i + 3];

    const luma = ToLuma(r, g, b);
    const ln = 1 - luma / 255.0;
    return ln;
  }
  
  Update(params: MagicParams) {

    let dRadius = GetRandomFloat(-MaxParticleSize / 5, MaxParticleSize / 5);
    const i = params.index;
    const r = params.phase;
    const d = params.d;
    // const d = 1 / Math.pow(10, params.d);
    // const d = 0.01;

    const t = params.t;

    // form spriral
    // this.x = Math.sin(r + i * d + t) * i + this.w / 2;
    // this.y = Math.cos(r + i * d + t) * i + this.h / 2;

    // form warp like
    // this.x = Math.sin(r + i * d + t) * i + this.w / 2;
    // this.y = Math.cos(r - i * d + t) * i + this.h / 2;

    // form paperlike
    // this.x = Math.sin(r + i * d + t) * i + this.w / 2;
    // this.y = Math.cos(r - i * d - t) * i + this.h / 2;

    // form random
    // this.x = Math.sin(r + i * d - t) * i + this.w / 2;
    // this.y = Math.cos(r - i * d + t) * i + this.h / 2;

    // form random
    this.x = Math.sin(r + this.x * d - t) * i + this.w / 2;
    this.y = Math.cos(r - this.y * d + t) * i + this.h / 2;

    // form more crazy random
    // this.x = Math.cos(r + Math.sin(r + this.x * d - t)) * i + this.w / 2;
    // this.y = Math.sin(r - Math.cos(r - this.y * d + t)) * i + this.h / 2;

    // this.x = Clamp(0, this.w - 1, this.x)
    // this.y = Clamp(0, this.h - 1, this.y)

    const ln = this.imageComplementLuma(params.imageData);
    const lt = (this.lifetime - this.ttl) / this.lifetime;

    let f = ln;
    this.alpha = ln;    

    this.radius += dRadius;    
    this.radius = Clamp(MinParticleSize, MaxParticleSize, this.radius) * f;

    if (this.speed < 1.0) {
      this.radius = 0.1;
    }
    // lifetime
    this.ttl += -1;
    if (this.ttl === 0) {
      this.reset();
    }
  }

  Draw(ctx: CanvasRenderingContext2D) {
    ctx.save();
    this.experiment3(ctx);
    ctx.restore();
  }
  experiment3(ctx:CanvasRenderingContext2D) {
    ctx.fillStyle = this.color;
    ctx.globalAlpha = this.alpha;
    let circle = new Path2D();
    circle.arc(this.x, this.y, this.radius, 0, Math.PI * 2);
    ctx.fill(circle);
  }

}