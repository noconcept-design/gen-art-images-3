////
export interface ISimObject {
  Update(params: MagicParams):void;
  Draw(ctx:CanvasRenderingContext2D):void;
}

export interface MagicParams {
  imageData: ImageData;
  index: number;
  phase: number;
  d: number;
  t: number;
}