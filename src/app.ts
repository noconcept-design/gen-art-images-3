import { MagicParams } from './isimobject';
import { Simulation } from './simulation';
import { GetRandomInt } from './utils';


function createDrawCanvas(imageCtx: CanvasRenderingContext2D, width: number, height: number) {
  const updateFrameRate = 60;
  const renderFrameRate = 60;

  // const canvas = document.getElementById('canvas') as HTMLCanvasElement;
  const canvas = document.createElement('canvas');
  // document.body.appendChild(canvas); 
  document.getElementById('canvas')?.appendChild(canvas);
  const ctx = canvas.getContext('2d');

  canvas.width = width;
  canvas.height = height;
  
  
  if (!canvas) return;
  if (!ctx) return;

  ctx.imageSmoothingEnabled = true;
  ctx.imageSmoothingQuality = 'high';

  const sim = new Simulation(width, height);
  const imageData = imageCtx.getImageData(0, 0, width, height);
  const params = <MagicParams>{
    imageData: imageData,
    phase: 0,
    //d: GetRandomInt(0, 4),
    d: 0,
    t: 0,
  };

  setInterval(() => {
    sim.Update(params);
  }, 1000 / updateFrameRate);

  setInterval(() => {
    sim.Draw(ctx)
  }, 1000 / renderFrameRate);  
}

function bootstrapper() {
  const width = 360;
  const height = 360;

  const imageCanvas = document.createElement('canvas');
  document.body.appendChild(imageCanvas);
  imageCanvas.width = width;
  imageCanvas.height = height;
  const ctx = imageCanvas.getContext('2d');
  if (!ctx) return;

  // image
  var image = new window.Image()
  if (!image) return;
  image.crossOrigin = 'Anonymous';
  image.onload = (e) => {
    ctx.drawImage(image, 0, 0, width, height);
    createDrawCanvas(ctx, width, height);
  }
  const images = [
    '../assets/images/tessy.jpg',
    '../assets/images/dhali.jpg',
    '../assets/images/monaspears.jpg',
    '../assets/images/zx10r-2004.jpg',
    '../assets/images/zx10r-logo.jpg',
    '../assets/images/fb.jpg',
    '../assets/images/yoji.jpg',
    '../assets/images/mandalorian.jpg',
    '../assets/images/buddha.jpg',
    '../assets/images/smiley.jpg'];
  image.src = images[GetRandomInt(0, images.length)];

} 
bootstrapper();